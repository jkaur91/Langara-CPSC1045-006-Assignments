//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function sliderXlistener(){
    // get the slider from my HTML 
    let sliderX=document.querySelector("#rangeX");

    // get the left circle from my HTML 
    // in CS = means assignment. So we assignment things on the right to the things on the left 
    
    let circle=document.querySelector("#circle");
    let circle1=document.querySelector("#circle1");
     
    // setAttribute means change attribute to a new value
    // attribute is cx
    // value here is sliderX.value 
    circle.setAttribute("r",sliderX.value);
    circle1.setAttribute("r",200 - sliderX.value );
    console.log(circle);
    
    // I have two circles with radius 100
    // total I have 200 in radius 
    // now if first circle radius becomes 50
    // then the other circle becomes 150 <--- 200 - 50
    // now let's say one is x
    // the other one should be 200 - x 
    // now lets say  is sliderX.value
    // the other one should be 200 - sliderX.value 
}


